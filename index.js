var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var cors = require('cors')


app.use(cors());
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

var kafka = require('kafka-node');
var ZOOKEEPER_SERVER = 'localhost:32181';
if (process.env.ZOOKEEPER_SERVER) {
  ZOOKEEPER_SERVER = process.env.ZOOKEEPER_SERVER;
}
var KAFKA_CLIENT_ID = "socketio-kafka";
console.log('ZOOKEEPER_SERVER: ' + ZOOKEEPER_SERVER);
var kafkaClient = new kafka.Client(ZOOKEEPER_SERVER, KAFKA_CLIENT_ID);
var consumer = new kafka.Consumer(kafkaClient, [], { autoCommit: true });

//load the topics
var TOPICS = 'twitter-sentiment';
if (process.env.TOPICS) {
  TOPICS = process.env.TOPICS;
}
console.log('TOPICS: ' + TOPICS);
var topicsArray = TOPICS.split(',');

consumer.addTopics(topicsArray, function (err, added) {
  console.log(err?err:added);
});

consumer.on('ready', function () { console.log('client ready!') });

consumer.on('message', function (message) {
  io.emit('twitter-socket', JSON.stringify(parseMessage(message)));
});

consumer.on('offsetOutOfRange', function (err) {
  console.log(err);
});

consumer.on('uncaughtException', function (err) {
    console.log('uncaughtException: ' + err);
}); 

http.listen(3000, function () {
  console.log('listening on *:3000');
});

var parseMessage = function (message) {
  return {
    key: ab2str(message.key),
    value: JSON.parse(message.value)
  }
}

var ab2str = function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
};
