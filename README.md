# SocketIo Consumer for Kafka 0.9 API
Socket Io with kafka consumer

#Image information
This Dockerfile uses the image cp-base, this image has the utilities for 
```
1. kafka-ready : Ensures a Kafka cluster is ready to accept client requests.
2. zk-ready: Ensures that a Zookeeper ensemble is ready to accept client requests.
3. sr-ready: Ensures that Schema Registry is ready to accept client requests.
4. kr-ready: Ensures that Kafka REST Proxy is ready to accept client requests.
5. listeners: Derives the listeners property from advertised.listeners.
```
That should be uses by for check if kafka is ready before start the application 
```
    command: "bash -c 'echo Waiting for Kafka to be ready...  && \
                       cub kafka-ready -b kafka:29092 1 20 && \
                       npm start'"
```
## Compile the Application and Build the image
```
$ npm install
$ docker build -t dataspartan/twitter-socket-consumer .
...
```